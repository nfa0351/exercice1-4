package dao;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Base64;

/**
 * import de opencsv pour écrire fichiers CSV
 */
import com.opencsv.*;

public class BadgeWalletDAO {

    /**
     * Classe BadgeWalletDAO avec simple constructeur par défaut
     * @param s
     */
    public BadgeWalletDAO(String s) {
    }

    /**
     * classe addBadge
     * @param image
     * @throws IOException
     */
    public void addBadge(File image) throws IOException {
        try {
            CSVWriter write = new CSVWriter(new FileWriter("src/test/resources/wallet.csv"));
            byte[] imageToByte = Files.readAllBytes(image.toPath());
            byte[] encodage = Base64.getEncoder().encode(imageToByte);
            String encodageToString = new String(encodage, StandardCharsets.UTF_8);
            String[] data = {encodageToString};
            write.writeNext(data);
            write.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * classe getBadge
     * @param imageStream
     * @throws IOException
     */
    public void getBadge(OutputStream imageStream) throws IOException {
        try {
            File image = new File("src/test/resources/petite_image.png");
            byte[] imageToByte = Files.readAllBytes(image.toPath());
            for (int i = 0; i < imageToByte.length-2; i++)
                imageStream.write(imageToByte[i]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}



